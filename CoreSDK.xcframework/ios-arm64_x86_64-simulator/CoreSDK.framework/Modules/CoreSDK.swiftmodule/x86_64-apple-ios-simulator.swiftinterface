// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.9 (swiftlang-5.9.0.128.108 clang-1500.0.40.1)
// swift-module-flags: -target x86_64-apple-ios13.0-simulator -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name CoreSDK
// swift-module-flags-ignorable: -enable-bare-slash-regex
import Combine
@_exported import CoreSDK
import Foundation
import Swift
import UIKit
import _Concurrency
import _StringProcessing
import _SwiftConcurrencyShims
public struct CommentData : Swift.Codable {
  public func getId() -> Swift.Int?
  public func getPostId() -> Swift.Int?
  public func getName() -> Swift.String?
  public func getEmail() -> Swift.String?
  public func getBody() -> Swift.String?
  public func encode(to encoder: any Swift.Encoder) throws
  public init(from decoder: any Swift.Decoder) throws
}
public struct PostData : Swift.Codable, Swift.Equatable {
  public static func == (lhs: CoreSDK.PostData, rhs: CoreSDK.PostData) -> Swift.Bool
  public mutating func appendComments(_ data: [CoreSDK.CommentData])
  public func getComments() -> [CoreSDK.CommentData]
  public func getId() -> Swift.Int?
  public func getUserId() -> Swift.Int?
  public func getTitle() -> Swift.String?
  public func getBody() -> Swift.String?
  public func encode(to encoder: any Swift.Encoder) throws
  public init(from decoder: any Swift.Decoder) throws
}
public typealias PostDataResponseCompletion = (_ posts: [CoreSDK.PostData], _ error: (any Swift.Error)?) -> Swift.Void
public typealias PostDataDetailResponseCompletion = (_ post: CoreSDK.PostData?, _ error: (any Swift.Error)?) -> Swift.Void
public typealias PostCommentsResponseCompletion = (_ comments: [CoreSDK.CommentData], _ error: (any Swift.Error)?) -> Swift.Void
public protocol BaseServiceProtocol {
  func updateExpirationTime(time: Foundation.TimeInterval)
  func getExpirationTime() -> Foundation.TimeInterval
  func updateHeaderParams(params: [Swift.String : Swift.String])
  func getHeaderParams() -> [Swift.String : Swift.String]
}
@_hasMissingDesignatedInitializers public class PostService {
  public init()
  public func updateExpirationTime(time: Foundation.TimeInterval)
  public func updateHeaderParams(params: [Swift.String : Swift.String])
  public func getExpirationTime() -> Foundation.TimeInterval
  public func getHeaderParams() -> [Swift.String : Swift.String]
  public func getAllPosts(completion: @escaping CoreSDK.PostDataResponseCompletion)
  public func getPostByIds(ids: [Swift.Int], completion: @escaping CoreSDK.PostDataResponseCompletion)
  public func getPostWithCommentByIds(ids: [Swift.Int], completion: @escaping CoreSDK.PostDataResponseCompletion)
  @objc deinit
}
extension CoreSDK.PostService : CoreSDK.BaseServiceProtocol {}
