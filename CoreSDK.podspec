Pod::Spec.new do |spec|

spec.name         = "CoreSDK"
spec.version      = "1.0.3"
spec.summary      = "CoreSDK is using for testing"
spec.description  = "Some of common using in Swift."

spec.platform     = :ios, "13.0"

spec.homepage     = "http://EXAMPLE/CoreSDK"
spec.license      = "MIT"
spec.author             = { "Quang Tran" => "tranquangbk56@gmail.com" }

spec.source       = { :path => "." }

spec.subspec 'CoreSDK' do |coresdk|
   coresdk.header_dir   =  'CoreSDK'
   coresdk.subspec 'XCFramework' do |xcframework|
      xcframework.vendored_frameworks = 'CoreSDK.xcframework'
   end
end

end
