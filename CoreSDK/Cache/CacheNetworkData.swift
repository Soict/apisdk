import Foundation

protocol CacheProtocol {
    func set(key: String, data: Data)
    func get(key: String, maxAge: TimeInterval) -> Data?
    func clear()
}

typealias DataCache = (data: Data, timeStamp: Date)

class CacheNetworkData: CacheProtocol {

    private var cache: [String: (data: Data, timestamp: Date)] = [:]

    init() {}

    func get(key: String, maxAge: TimeInterval) -> Data? {
        if let cachedData = self.cache[key], Date().timeIntervalSince(cachedData.timestamp) < maxAge {
            return cachedData.data
        } else {
            return nil
        }
    }

    func set(key: String, data: Data) {
        self.cache[key] = (data, Date())
    }
    
    func clear() {
        self.cache = [:]
    }
}
