//
//  APIDefine.swift
//  CoreSDK
//
//  Created by Tran Van Quang on 04/01/2024.
//

import UIKit

struct APIDefine {
    
    static var cacheExpirationTime: TimeInterval        =   5 * 60  //5 mins
    
    static var listPosts: String                        =   "/posts"
    static var detailPost: String                       =   "/posts/%d"
    static var commentsOfPost: String                   =   "/posts/%d/comments"
    
}
