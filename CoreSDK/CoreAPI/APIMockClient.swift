
import UIKit

class APIMockClient: APIClientProtocol {
    
    private var expirationTime: TimeInterval        =   0.0
    private var headerParams: [String: String]      =   [:]

    func updateExpirationTime(_ time: TimeInterval) {
        self.expirationTime = time
    }
    
    func getExpirationTime() -> TimeInterval {
        return self.expirationTime
    }
    
    func updateHeaderParams(_ params: [String : String]) {
        self.headerParams = params
    }
    
    func getHeaderParams() -> [String : String] {
        return self.headerParams
    }
    
    func request<T>(path: String, method: APIMethod, body_params: [String : Any], completion: @escaping (Result<[T], Error>) -> Void) where T : Decodable {
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
            let decoder = JSONDecoder()
            
            if let items = try? decoder.decode([T].self, from: data) {
                completion(.success(items))
                return
            }
            
            // Attempt to decode as a single object
            let singleItem = try decoder.decode(T.self, from: data)
            completion(.success([singleItem]))
        } catch {
            completion(.failure(APIError.decodingError))
        }
    }
}
