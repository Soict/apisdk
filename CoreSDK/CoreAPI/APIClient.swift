//
//  APIClientManager.swift
//  CoreSDK
//
//  Created by Tran Van Quang on 04/01/2024.
//

import UIKit

enum APIMethod: String {
    case GET, POST
}

enum APIError: Error {
    case invalidURL
    case noData
    case decodingError
    case encodingError
}

protocol APIClientProtocol {
    func updateExpirationTime(_ time: TimeInterval)
    func getExpirationTime() -> TimeInterval
    func updateHeaderParams(_ params: [String: String])
    func getHeaderParams() -> [String: String]
    
    func request<T: Decodable>(path: String,
                               method: APIMethod,
                               body_params: [String: Any],
                               completion: @escaping (Result<[T], Error>) -> Void)
}

class APIClient: APIClientProtocol {
        
    private var BASE_URL: String                =   "https://jsonplaceholder.typicode.com"
    private var cache: CacheProtocol            =   CacheNetworkData()
    private var expirationTime: TimeInterval    =   APIDefine.cacheExpirationTime
    private var headerParams: [String: String]  =   [:]
    
    func updateExpirationTime(_ time: TimeInterval) {
        self.expirationTime = time
    }
    
    func getExpirationTime() -> TimeInterval {
        return self.expirationTime
    }
    
    func updateHeaderParams(_ params: [String: String]) {
        self.headerParams = params
    }
    
    func getHeaderParams() -> [String : String] {
        return self.headerParams
    }

    func request<T: Decodable>(path: String,
                               method: APIMethod,
                               body_params: [String: Any],
                               completion: @escaping (Result<[T], Error>) -> Void) {
        if let cachedData = self.cache.get(key: path, maxAge: expirationTime) {
            print("Using cached data")
            self.responseData(cachedData, completion: completion)
        } else {
            guard let url = URL(string: "\(BASE_URL)\(path)") else {
                completion(.failure(APIError.invalidURL))
                return
            }
            
            var request = URLRequest(url: url)
            request.setValue("application/json", forHTTPHeaderField: "Accept")
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            
            //Append header params if has
            for (key, value) in self.headerParams {
                request.setValue(value, forHTTPHeaderField: key)
            }
            request.httpMethod = method.rawValue
            if !body_params.isEmpty {
                do {
                    // Convert parameters to JSON data and set as the request body
                    request.httpBody = try JSONSerialization.data(withJSONObject: body_params, options: .prettyPrinted)
                } catch {
                    completion(.failure(APIError.encodingError))
                }
            }
            
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                if let error = error {
                    completion(.failure(error))
                    return
                }
                guard let responseData = data else {
                    completion(.failure(APIError.noData))
                    return
                }
                
                self.cache.set(key: path, data: responseData)
                self.responseData(responseData, completion: completion)
            }
            
            task.resume()
        }
    }
    
    private func responseData<T: Decodable>(_ data: Data, completion: @escaping (Result<[T], Error>) -> Void) {
        do {
            let decoder = JSONDecoder()
            
            if let items = try? decoder.decode([T].self, from: data) {
                completion(.success(items))
                return
            }
            
            // Attempt to decode as a single object
            let singleItem = try decoder.decode(T.self, from: data)
            completion(.success([singleItem]))
            
        } catch {
            completion(.failure(APIError.decodingError))
        }
    }
}
