import UIKit

//Comment data model
public struct CommentData: Codable {

    var postId: Int?
    var id: Int?
    var name: String?
    var email: String?
    var body: String?
    
    enum CodingKeys: String, CodingKey {
        case postId, id, name, email, body
    }
    
    public func getId() -> Int? {
        return self.id
    }
    
    public func getPostId() -> Int? {
        return self.postId
    }
    
    public func getName() -> String? {
        return self.name
    }
    
    public func getEmail() -> String? {
        return self.email
    }
    
    public func getBody() -> String? {
        return self.body
    }
}
