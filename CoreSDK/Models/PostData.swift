
import UIKit

//Post data model
public struct PostData: Codable, Equatable {
    public static func == (lhs: PostData, rhs: PostData) -> Bool {
        return  lhs.id == rhs.id &&
                lhs.userId == rhs.userId &&
                lhs.title == rhs.title &&
                lhs.body == rhs.body &&
                lhs.getComments().count == rhs.getComments().count
    }
    
    var userId: Int?
    var id: Int?
    var title: String?
    var body: String?
    
    private var comments: [CommentData] = []
    
    enum CodingKeys: String, CodingKey {
        case userId, id, title, body
    }
    
    mutating public func appendComments(_ data: [CommentData]) {
        self.comments.append(contentsOf: data)
    }
    
    public func getComments() -> [CommentData] {
        return self.comments
    }
    
    public func getId() -> Int? {
        return self.id
    }
    
    public func getUserId() -> Int? {
        return self.userId
    }
    
    public func getTitle() -> String? {
        return self.title
    }
    
    public func getBody() -> String? {
        return self.body
    }
}
