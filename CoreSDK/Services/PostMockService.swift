//
//  PostMockService.swift
//  CoreSDK
//
//  Created by Tran Van Quang on 05/01/2024.
//

import UIKit

class PostMockService: PostServiceProtocol {
    
    private var apiClient: APIClientProtocol!
    
    public init() {
        self.apiClient = APIMockClient()
    }
    
    public init(apiClient: APIClientProtocol) {
        self.apiClient = apiClient
    }
    
    public func updateExpirationTime(time: TimeInterval) {
        self.apiClient.updateExpirationTime(time)
    }
    
    public func updateHeaderParams(params: [String : String]) {
        self.apiClient.updateHeaderParams(params)
    }
    
    public func getExpirationTime() -> TimeInterval {
        return self.apiClient.getExpirationTime()
    }
    
    public func getHeaderParams() -> [String: String] {
        return self.apiClient.getHeaderParams()
    }
    
    func getAllPosts(completion: @escaping PostDataResponseCompletion) {
        guard let path = Bundle(identifier: "com.test.CoreSDKTests")?.path(forResource: "posts_data", ofType: "json") else {
            completion([], nil)
            return
        }
        self.apiClient.request(path: path, method: .GET, body_params: [:]) { (result: Result<[PostData], Error>) in
            switch result {
            case .success(let data):
                completion(data, nil)
            case .failure(let error):
                completion([], error)
            }
        }
    }
    
    func getPostByIds(ids: [Int], completion: @escaping PostDataResponseCompletion) {
        let group = DispatchGroup()
        var posts = [PostData]()
        
        DispatchQueue.global(qos: .background).async {
            for id in ids {
                group.enter()
                self.getPostById(id: id) { post, error in
                    if let post = post {
                        posts.append(post)
                    }
                    group.leave()
                }
            }
            group.notify(queue: .main) {
                completion(posts, nil)
            }
        }
    }
    
    func getPostWithCommentByIds(ids: [Int], completion: @escaping PostDataResponseCompletion) {
        let group = DispatchGroup()
        var posts = [PostData]()

        self.getPostByIds(ids: ids) { data, error in
            
            posts.append(contentsOf: data)
            
            DispatchQueue.global(qos: .background).async {
                for index in 0..<posts.count {
                    var post = posts[index]
                    guard let postId = post.id, postId > 0 else { continue }
                    group.enter()
                    self.getCommentsByPostId(id: postId) { comments, error in
                        post.appendComments(comments)
                        posts[index] = post
                        group.leave()
                    }
                }
                
                group.notify(queue: .main) {
                    completion(posts, nil)
                }
            }
        }
    }
    
    func getPostById(id: Int, completion: @escaping PostDataDetailResponseCompletion) {
        guard let path = Bundle(identifier: "com.test.CoreSDKTests")?.path(forResource: "posts_data_\(id)", ofType: "json") else {
            completion(nil, nil)
            return
        }
        self.apiClient.request(path: path, method: .GET, body_params: [:]) { (result: Result<[PostData], Error>) in
            switch result {
            case .success(let data):
                completion(data.first, nil)
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    
    func getCommentsByPostId(id: Int, completion: @escaping PostCommentsResponseCompletion) {
        guard let path = Bundle(identifier: "com.test.CoreSDKTests")?.path(forResource: "posts_data_comments_\(id)", ofType: "json") else {
            completion([], nil)
            return
        }
        self.apiClient.request(path: path, method: .GET, body_params: [:]) { (result: Result<[CommentData], Error>) in
            switch result {
            case .success(let data):
                completion(data, nil)
            case .failure(let error):
                completion([], error)
            }
        }
    }
}
