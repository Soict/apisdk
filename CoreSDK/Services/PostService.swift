//
//  APIClientHandler.swift
//  CoreSDK
//
//  Created by Tran Van Quang on 04/01/2024.
//

import UIKit
import Combine

public typealias PostDataResponseCompletion = (_ posts: [PostData], _ error: Error?) -> Void
public typealias PostDataDetailResponseCompletion = (_ post: PostData?, _ error: Error?) -> Void
public typealias PostCommentsResponseCompletion = (_ comments: [CommentData], _ error: Error?) -> Void

public protocol BaseServiceProtocol {
    func updateExpirationTime(time: TimeInterval)
    func getExpirationTime() -> TimeInterval
    func updateHeaderParams(params: [String: String])
    func getHeaderParams() -> [String: String]
}

protocol PostServiceProtocol: BaseServiceProtocol {
    
    func getAllPosts(completion: @escaping PostDataResponseCompletion)
    func getPostByIds(ids: [Int], completion: @escaping PostDataResponseCompletion)
    func getPostWithCommentByIds(ids: [Int], completion: @escaping PostDataResponseCompletion)
    
}

public class PostService: PostServiceProtocol {
    
    private var apiClient: APIClientProtocol!
    
    public init() {
        self.apiClient = APIClient()
    }
    
    init(apiClient: APIClientProtocol) {
        self.apiClient = apiClient
    }
    
    /**
     Update expirationTime for cache and header params if needed.
     */
    public func updateExpirationTime(time: TimeInterval) {
        self.apiClient.updateExpirationTime(time)
    }
    
    public func updateHeaderParams(params: [String : String]) {
        self.apiClient.updateHeaderParams(params)
    }
    
    /**
     Get expiration Time for cache
     */
    public func getExpirationTime() -> TimeInterval {
        return self.apiClient.getExpirationTime()
    }
    
    /**
     Get header params if has
     */
    public func getHeaderParams() -> [String: String] {
        return self.apiClient.getHeaderParams()
    }
    
    /**
     Get all posts in the server. Return an array of posts
     */
    public func getAllPosts(completion: @escaping PostDataResponseCompletion) {
        self.apiClient.request(path: APIDefine.listPosts, method: .GET, body_params: [:]) { (result: Result<[PostData], Error>) in
            switch result {
            case .success(let data):
                completion(data, nil)
            case .failure(let error):
                completion([], error)
            }
        }
    }
    
    /**
     Get posts by ids from server.
     */
    public func getPostByIds(ids: [Int], completion: @escaping PostDataResponseCompletion) {
        let group = DispatchGroup()
        var posts = [PostData]()
        
        DispatchQueue.global(qos: .background).async {
            for id in ids {
                group.enter()
                self.getPostById(id: id) { post, error in
                    if let post = post {
                        posts.append(post)
                    }
                    group.leave()
                }
            }
            group.notify(queue: .main) {
                completion(posts, nil)
            }
        }
    }
    
    /**
     Get posts and post's comments by ids from server.
     */
    public func getPostWithCommentByIds(ids: [Int], completion: @escaping PostDataResponseCompletion) {
        let group = DispatchGroup()
        var posts = [PostData]()

        self.getPostByIds(ids: ids) { data, error in
            
            posts.append(contentsOf: data)
            
            DispatchQueue.global(qos: .background).async {
                for index in 0..<posts.count {
                    var post = posts[index]
                    guard let postId = post.id, postId > 0 else { continue }
                    group.enter()
                    self.getCommentsByPostId(id: postId) { comments, error in
                        post.appendComments(comments)
                        posts[index] = post
                        group.leave()
                    }
                }
                
                group.notify(queue: .main) {
                    completion(posts, nil)
                }
            }
        }
    }
    
    /**
     Get post detail by post Id from server.
     */
    private func getPostById(id: Int, completion: @escaping PostDataDetailResponseCompletion) {
        let path = String(format: "\(APIDefine.detailPost)", id)
        self.apiClient.request(path: path, method: .GET, body_params: [:]) { (result: Result<[PostData], Error>) in
            switch result {
            case .success(let data):
                completion(data.first, nil)
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    
    /**
     Get all comments from a post by post Id from server.
     */
    private func getCommentsByPostId(id: Int, completion: @escaping PostCommentsResponseCompletion) {
        let path = String(format: "\(APIDefine.commentsOfPost)", id)
        self.apiClient.request(path: path, method: .GET, body_params: [:]) { (result: Result<[CommentData], Error>) in
            switch result {
            case .success(let data):
                completion(data, nil)
            case .failure(let error):
                completion([], error)
            }
        }
    }
}
