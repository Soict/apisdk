import XCTest
@testable import CoreSDK

final class PostService_Test: XCTestCase {
    
    private var apiClient: APIMockClient!
    private var service: PostMockService!

    private func getDataFrom<T: Decodable>(path: String,
                                           completion: @escaping (Result<[T], Error>) -> Void) {
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
            let decoder = JSONDecoder()
            
            if let items = try? decoder.decode([T].self, from: data) {
                completion(.success(items))
                return
            }
            // Attempt to decode as a single object
            let singleItem = try decoder.decode(T.self, from: data)
            completion(.success([singleItem]))
        } catch {
            completion(.failure(APIError.decodingError))
        }
    }
    
    override func setUp() {
        apiClient = APIMockClient()
        service = PostMockService(apiClient: apiClient)
    }
    
    func testUpdateExpirationCacheTimeSuccess() {
        // Given
        let exceptedTime: TimeInterval = 1 * 60 //60 seconds
        // When
        service.updateExpirationTime(time: exceptedTime)
        // Then
        XCTAssertEqual(service.getExpirationTime(), exceptedTime)
    }
    
    func testUpdateExpirationCacheTimeFailed() {
        // Given
        let exceptedTime: TimeInterval = 1 * 60 //60 seconds
        // When.
        service.updateExpirationTime(time: 10)
        // Then
        XCTAssertNotEqual(service.getExpirationTime(), exceptedTime)
    }
    
    func testUpdateHeaderParamsSuccess() {
        // Given
        let exceptedParams: [String: String] = ["access_token": "Test"]
        // When
        service.updateHeaderParams(params: exceptedParams)
        // Then
        XCTAssertEqual(service.getHeaderParams(), exceptedParams)
    }
    
    func testUpdateHeaderParamsFailed() {
        // Given
        let exceptedParams: [String: String] = ["access_token": "Test"]
        // When.
        service.updateHeaderParams(params: [:])
        // Then
        XCTAssertNotEqual(service.getHeaderParams(), exceptedParams)
    }
    
    func testGetAllPostsSuccess() {
        // Given
        let path = Bundle(identifier: "com.test.CoreSDKTests")?.path(forResource: "posts_data", ofType: "json") ?? ""
        var expectedData: [PostData] = []
        self.getDataFrom(path: path) { (result: Result<[PostData], Error>) in
            switch result {
            case .success(let data):
                expectedData = data
            case .failure(_):
                break
            }
        }
        
        // When
        var data: [PostData] = []
        service.getAllPosts { posts, error in
            data = posts
        }
        // Then
        XCTAssertEqual(data.count, expectedData.count)
    }
    
    func testGetAllPostsFailed() {
        // Given
        let path = Bundle(identifier: "com.test.CoreSDKTests")?.path(forResource: "posts", ofType: "json") ?? ""
        var expectedData: [PostData] = []
        self.getDataFrom(path: path) { (result: Result<[PostData], Error>) in
            switch result {
            case .success(let data):
                expectedData = data
            case .failure(_):
                break
            }
        }
        
        // When
        var data: [PostData] = []
        service.getAllPosts { posts, error in
            data = posts
        }
        
        // Then
        XCTAssertNotEqual(expectedData.count, data.count)
    }
    
    func testGetPostByIdSuccess() {
        // Given
        let postId: Int = 1
        let path = Bundle(identifier: "com.test.CoreSDKTests")?.path(forResource: "posts_data_\(postId)", ofType: "json") ?? ""
        var expectedData: PostData?
        self.getDataFrom(path: path) { (result: Result<[PostData], Error>) in
            switch result {
            case .success(let data):
                expectedData = data.first
            case .failure(_):
                break
            }
        }
        // When
        var post: PostData?
        service.getPostById(id: postId) { data, error in
            post = data
        }
        // Then
        XCTAssertNotNil(post)
        XCTAssertEqual(post, expectedData)
    }
    
    func testGetPostByIdFailed() {
        // Given
        let postId: Int = 0
        let expectedData: PostData? = nil
        // When
        var post: PostData?
        service.getPostById(id: postId) { data, error in
            post = data
        }
        // Then
        XCTAssertNil(post)
        XCTAssertEqual(post, expectedData)
    }
    
    func testGetPostsByIdsSuccess() {
        // Given
        let ids: [Int] = [1, 2]
        let paths = ids.map({ Bundle(identifier: "com.test.CoreSDKTests")?.path(forResource: "posts_data_\($0)", ofType: "json") ?? "" })
        var expectedData: [PostData] = []
        for path in paths {
            self.getDataFrom(path: path) { (result: Result<[PostData], Error>) in
                switch result {
                case .success(let data):
                    expectedData.append(contentsOf: data)
                case .failure(_):
                    break
                }
            }
        }
        // When
        let expectation = XCTestExpectation(description: "Fetching posts by ids")
        var data: [PostData] = []
        service.getPostByIds(ids: ids) { posts, error in
            data = posts
            expectation.fulfill()
        }
        // Then
        wait(for: [expectation], timeout: 2.0)
        XCTAssertEqual(data.count, expectedData.count)
    }
    
    func testGetPostsByIdsFailed() {
        // Given
        let ids: [Int] = [4, 5]
        let expectedData: [PostData] = []
        // When
        let expectation = XCTestExpectation(description: "Fetching posts by ids")
        var data: [PostData] = []
        service.getPostByIds(ids: ids) { posts, error in
            data = posts
            expectation.fulfill()
        }
        // Then
        wait(for: [expectation], timeout: 2.0)
        XCTAssertEqual(data.count, expectedData.count)
    }
    
    func testGetCommentsByPostIdSuccess() {
        // Given
        let postId: Int = 1
        let path = Bundle(identifier: "com.test.CoreSDKTests")?.path(forResource: "posts_data_comments_\(postId)", ofType: "json") ?? ""
        var expectedData: [CommentData] = []
        self.getDataFrom(path: path) { (result: Result<[CommentData], Error>) in
            switch result {
            case .success(let data):
                expectedData = data
            case .failure(_):
                break
            }
        }
        // When
        var data: [CommentData] = []
        service.getCommentsByPostId(id: postId) { comments, error in
            data = comments
        }
        // Then
        XCTAssertTrue(data.count == expectedData.count)
    }
    
    func testGetCommentsByPostIdFailed() {
        // Given
        let postId: Int = 0
        let expectedData: [CommentData] = []
        // When
        var data: [CommentData] = []
        service.getCommentsByPostId(id: postId) { comments, error in
            data = comments
        }
        // Then
        XCTAssertTrue(data.count == expectedData.count)
    }
    
    func testGetPostsWithCommentsByIdsSuccess() {
        // Given
        let ids: [Int] = [1, 2]
        // When
        let expectation = XCTestExpectation(description: "Fetching posts with comments by ids")
        var data: [PostData] = []
        service.getPostWithCommentByIds(ids: ids) { posts, error in
            data = posts
            expectation.fulfill()
        }
        // Then
        wait(for: [expectation], timeout: 2.0)
        XCTAssertEqual(data.map({ $0.getId() }), ids)
    }
    
    func testGetPostsWithCommentsByIdsFailed() {
        // Given
        let ids: [Int] = [4, 5]
        // When
        let expectation = XCTestExpectation(description: "Fetching posts with comments by ids")
        var data: [PostData] = []
        service.getPostWithCommentByIds(ids: ids) { posts, error in
            data = posts
            expectation.fulfill()
        }
        // Then
        wait(for: [expectation], timeout: 2.0)
        XCTAssertNotEqual(data.map({ $0.getId() }).count, ids.count)
    }
}
