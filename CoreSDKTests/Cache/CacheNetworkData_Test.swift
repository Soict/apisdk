//
//  CacheNetworkData_Test.swift
//  CoreSDKTests
//
//  Created by Tran Van Quang on 05/01/2024.
//

import XCTest
@testable import CoreSDK

class CacheNetworkData_Test: XCTestCase {

    private var cache: CacheProtocol!
    
    override func setUp() {
        cache = CacheNetworkData()
    }
    
    func testGetCacheDataWithInExpirationTimeSuccess() {
        // Given
        let expectedData: String = "Data cache"
        let key: String  = "Data_key"
        let dataCache: Data = expectedData.data(using: .utf8) ?? Data()
        let expirationTime: TimeInterval = 4.0
        // When
        let expectation = XCTestExpectation(description: "Cache data in 3 second")
        self.cache.set(key: key, data: dataCache)
        DispatchQueue.main.asyncAfter(deadline: .now() + expirationTime - 1.0) {
            expectation.fulfill()
        }
        // Then
        wait(for: [expectation], timeout: expirationTime)
        let cached = self.cache.get(key: key, maxAge: expirationTime)
        XCTAssertNotNil(cached)
        XCTAssertEqual(dataCache, cached)
    }
    
    func testGetCacheDataWithInExpirationTimeFailed() {
        // Given
        let expectedData: String = "Data cache"
        let key: String  = "Data_key"
        let dataCache: Data = expectedData.data(using: .utf8) ?? Data()
        let expirationTime: TimeInterval = 4.0
        // When
        let expectation = XCTestExpectation(description: "Cache data in 5 second")
        self.cache.set(key: key, data: dataCache)
        DispatchQueue.main.asyncAfter(deadline: .now() + expirationTime + 1.0) {
            expectation.fulfill()
        }
        // Then
        wait(for: [expectation], timeout: expirationTime + 1.0)
        let cached = self.cache.get(key: key, maxAge: expirationTime)
        XCTAssertNil(cached)
    }
    
    func testClearCacheDataSuccess() {
        // Given
        let expectedData: String = "Data cache"
        let key: String  = "Data_key"
        let dataCache: Data = expectedData.data(using: .utf8) ?? Data()
        let expirationTime: TimeInterval = 4.0
        // When
        self.cache.set(key: key, data: dataCache)
        self.cache.clear()
        // Then
        let cached = self.cache.get(key: key, maxAge: expirationTime)
        XCTAssertNil(cached)
    }
}
